# Lempel–Ziv–Welch (LZW) algorithm implementation in Raku

## Synopsis

```perl
my int $r = 0;
my str $d = 'Тексты - это не энциклопедические и не лингвистические системы.' x 255;

# create LWZ object with default dictionary size (97000)
my $lzw = LZW::Revolunet.new;

# OR create LWZ object with user defined dictionary size (be careful)
my $lzw = LZW::Revolunet.new(:dictsize(200000));

# compress
my $cmp  = $lzw.compress(:s($lzw.encode_utf8(:s($d))));

# get compression ratio
$r = $lzw.get_ratio;

# decompress
my $dcmp = $lzw.decode_utf8(:s($lzw.decompress(:s($cmp))));

# validate
if !($dcmp eq $d) {
    die 'decompressed data is corrupted';
}

# compress/decompress statistics
('compression ratio ' ~ $r ~ '%').say;
```

## Description

**LZW::Revolunet** — [Raku](https://raku.org) implementation of universal lossless data compression [algorithm](https://en.wikipedia.org/wiki/Lzw) created by Abraham Lempel, Jacob Ziv, and Terry Welch. This module is based on JavaScript implementation ([lzw_encoder.js](https://gist.github.com/revolunet/843889)) by [Julien Bouquillon](https://github.com/revolunet)

## License

**LZW::Revolunet** is free and opensource software, so you can redistribute it and/or modify it under the terms of the [The Artistic License 2.0](https://opensource.org/licenses/Artistic-2.0).

## Author

Please contact me via [LinkedIn](https://www.linkedin.com/in/knarkhov/) or [Twitter](https://twitter.com/CondemnedCell). Your feedback is welcome at [narkhov.pro](https://narkhov.pro/contact-information.html).

## See also

[LZW Data Compression](https://www2.cs.duke.edu/csed/curious/compression/lzw.html)
