/*
 * This code was taken from https://gist.github.com/revolunet/843889
 * Thanks: @revolunet, @lerouxb, @HughMeyers and others in comments!
 */

var DSZ=57344;
LZW = {
    compress: function (s) {
        var dict = {};
        var data = (s + "").split("");
        var out = [];
        var currChar;
        var phrase = data[0];
        var code = DSZ;
        for (var i=1; i<data.length; i++) {
            currChar=data[i];
            if (dict['_' + phrase + currChar] != null) {
                phrase += currChar;
            }
            else {
                out.push(phrase.length > 1 ? dict['_'+phrase] : phrase.charCodeAt(0));
                dict['_' + phrase + currChar] = code;
                code++;
                phrase=currChar;
            }
        }
        out.push(phrase.length > 1 ? dict['_'+phrase] : phrase.charCodeAt(0));
        for (var i=0; i<out.length; i++) {
            out[i] = String.fromCharCode(out[i]);
        }
        return out.join("");
    },
    decompress: function (s) {
        var dict = {};
        var data = (s + "").split("");
        var currChar = data[0];
        var oldPhrase = currChar;
        var out = [currChar];
        var code = DSZ;
        var phrase;
        for (var i=1; i<data.length; i++) {
            var currCode = data[i].charCodeAt(0);
            if (currCode < DSZ) {
                phrase = data[i];
            }
            else {
               phrase = dict['_'+currCode] ? dict['_'+currCode] : (oldPhrase + currChar);
            }
            out.push(phrase);
            currChar = phrase.charAt(0);
            dict['_'+code] = oldPhrase + currChar;
            code++;
            oldPhrase = phrase;
        }
        return out.join("");
    },
    encode_utf8: function (s) {
      return unescape(encodeURIComponent(s));
    },
    decode_utf8: function (s) {
      return decodeURIComponent(escape(s));
    }
}
