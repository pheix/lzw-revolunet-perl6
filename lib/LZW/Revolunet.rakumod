unit class LZW::Revolunet;

has int  $!ratio          = 0;
has uint $.dictsize is rw = 97000;
has Bool $.debug    is default(False);

method get_ratio returns int {
    $!ratio
}

method !count_bytes(uint :$code!) returns uint {
    my uint $b;

    if $code < 256  {
        $b++;
    }
    else {
        my uint $x = $code;
        my uint $n = 0;

        while !( $x == 0 ) {
            $x = $x +> 8;
            $n++;
        }

        $b += $n;
    }

    $b;
}

method get_bytes(str :$s!) returns uint {
    my uint $bytes;
    my @out = $s.split(q{},:skip-empty);

    for @out -> $char {
        $bytes += self!count_bytes(:code($char.ord));
    }

    $bytes;
}

method encode_utf8(str :$s!) returns str {
    my $rs;

    if $s {
        $rs = $s.encode.decode("iso-8859-1");
    }

    $rs;
}

method decode_utf8(str :$s!) returns str {
    my $rs;

    if $s {
        $rs = $s.encode("iso-8859-1").decode;
    }

    $rs;
}

method compress(str :$s!) returns str {
    my %dict;
    my @out;
    my $currChar;

    $!ratio    = 0;
    my uint $c = 0;
    my uint $i = 0;
    my @data   = $s.split(q{}, :skip-empty);
    my $phrase = @data[0];
    my $code   = $!dictsize;

    for (1 .. (@data.elems - 1)) -> $char {
        $currChar = @data[$char];

        if %dict{ '_' ~ $phrase ~ $currChar }:exists {
            $phrase ~= $currChar;
        } else {
            @out.push(
                $phrase.chars > 1 ??
                %dict{ '_' ~ $phrase } !!
                ($phrase.split(q{},:skip-empty))[0].ord
            );

            %dict{ '_' ~ $phrase ~ $currChar } = $code;

            $code++;
            $phrase = $currChar;
        }
    }

    @out.push(
        $phrase.chars > 1 ??
        %dict{ '_' ~ $phrase } !!
        ($phrase.split(q{},:skip-empty))[0].ord
    );

    for @out -> $o {
        $c      += self!count_bytes(:code($o));
        @out[$i] = $o.chr;

        $i++;
    }

    if $!debug {
        ('original: ' ~ self.get_bytes(:s($s))).say;
        ('compress: ' ~ self.get_bytes(:s(@out.join(q{})))).say;
    }

    my $encbytes = $s.encode("iso-8859-1").bytes;

    if $encbytes > 0 {
        $!ratio = floor (100 - $c/$encbytes*100);
    }

    @out.join(q{});
}

method decompress(str :$s!) returns str {
    my %dict;
    my @out;
    my $phrase;

    my uint $i    = 0;
    my @data      = $s.split(q{}, :skip-empty);
    my $currChar  = @data[0];
    my $oldPhrase = $currChar;
    my $code      = $!dictsize;

    @out.push($currChar);

    for (1 .. (@data.elems - 1)) -> $char {
        my $currCode = @data[$char].ord;
        if $currCode < $!dictsize {
            $phrase = @data[$char];
        } else {
            $phrase =
                %dict{ '_' ~ $currCode } ??
                %dict{ '_' ~ $currCode } !! ( $oldPhrase ~ $currChar );
        }

        @out.push($phrase);

        $currChar            = ($phrase.split(q{},:skip-empty))[0];
        %dict{ '_' ~ $code } = $oldPhrase ~ $currChar;

        $code++;
        $oldPhrase = $phrase;
    }

    @out.join(q{});
}
