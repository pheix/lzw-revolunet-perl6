use v6.c;
use Test;

use LZW::Revolunet;

subtest {
    my int $r = 0;
    my str $d = 'Тексты - это не энциклопедические и не лингвистические системы.' x 255;

    # create LWZ object with default dictionary size (97000)
    # my $lzw = LZW::Revolunet.new;

    # OR create LWZ object with user defined dictionary size (be careful)
    my $lzw = LZW::Revolunet.new(:dictsize(200000));

    # compress
    my $cmp  = $lzw.compress(:s($lzw.encode_utf8(:s($d))));

    # get compression ratio
    $r = $lzw.get_ratio;

    # decompress
    my $dcmp = $lzw.decode_utf8(:s($lzw.decompress(:s($cmp))));

    # validate
    ok $dcmp eq $d, 'decompressed data is valid';

    # compress/decompress statistics
    diag('compression ratio ' ~ $r ~ '%');

    ok $r > 0, 'compression ratio is positive';
}, 'subtest from README.md';

done-testing;
