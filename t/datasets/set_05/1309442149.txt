<h1>
	<strong><span style="color:#800000;">ПРОДАЖА И ОФОРМЛЕНИЕ ОБЪЕКТОВ </span></strong></h1>
<h1>
	<strong><span style="color:#800000;">КОММЕРЧЕСКОЙ НЕДВИЖИМОСТИ</span></strong></h1>
<p>
	&nbsp;</p>
<p style="text-align: justify">
	<strong><span style="color:#800000;">Компания ООО &laquo;Архистрой&raquo;&nbsp;</span></strong>оказывает услуги в области сопровождения, оформления документов и операций купли - продажи обектов коммерческой недвижимости. Также компания занимается продажей обектов коммерческой недвижимости в Ярославле и Ярославской области. <a href="//990220.ru/simplecontacts.html">Специалисты компании</a> <span style="color:#800000;"><strong>ООО &laquo;Архистрой&raquo;</strong></span> окажут консультации, по операциями с обектами коммерческой недвижимости.&nbsp;</p>
<p>
	&nbsp;</p>
<p>
	<span style="font-size:18px;"><span style="color:#ff0000;"><strong>КОММЕРЧЕСКОЕ ПРЕДЛОЖЕНИЕ&nbsp;</strong></span></span></p>
<p>
	&nbsp;</p>
<p>
	<strong><span style="color:#800000;">ГОСТИННИЦА - ОБЩЕЖИТИЕ </span></strong>с.Горелово, Брейтовский р-он</p>
<p>
	<strong>Цена продажи (как есть), руб. : 4 500 000&nbsp;</strong></p>
<table border="0" cellpadding="1" cellspacing="1" style="width: 500px">
	<tbody>
		<tr>
			<td>
				<img alt="" src="//990220.ru/images/gallery/upload/1309445145_0.jpg" style="height: 225px; width: 300px; margin-left: 5px; margin-right: 5px; margin-top: 5px; margin-bottom: 5px" /></td>
			<td>
				<img alt="" src="//990220.ru/images/gallery/upload/1309445145_1.jpg" style="height: 225px; width: 300px; margin-left: 5px; margin-right: 5px; margin-top: 5px; margin-bottom: 5px" /></td>
		</tr>
	</tbody>
</table>
<p>
	&nbsp;</p>
<table border="1" cellpadding="2" cellspacing="0">
	<tbody>
		<tr>
			<td style="width: 218px; background-color: rgb(204, 204, 255)">
				<div style="text-align: center">
					<strong>Месторасположение</strong></div>
			</td>
			<td style="width: 407px; background-color: rgb(204, 204, 255)">
				<div style="text-align: justify">
					<strong>Ярославская область</strong></div>
			</td>
		</tr>
		<tr>
			<td style="width:218px;">
				<div style="text-align: justify">
					<strong>Район</strong></div>
			</td>
			<td style="width:407px;">
				<div style="text-align: justify">
					Брейтовский</div>
			</td>
		</tr>
		<tr>
			<td style="width: 218px; background-color: rgb(204, 204, 255)">
				<div style="text-align: justify">
					<strong>Близлежащий населенный пункт</strong></div>
			</td>
			<td style="width: 218px; background-color: rgb(204, 204, 255)">
				<div style="text-align: justify">
					с. Горелово (территория природного заказника) расстояние до р. Чеснова 500 м, до Рыбинского водохранилища 800 м.</div>
			</td>
		</tr>
		<tr>
			<td style="width:218px;">
				<div style="text-align: justify">
					<strong>Расстояние до объекта от основной трассы</strong></div>
			</td>
			<td style="width:407px;">
				<div style="text-align: justify">
					30-50 метров (Хороший подъезд) от трассы, &nbsp;15 км от райцентра и 376 км. от МКАД г.Москва от Ярославля около 200 км.</div>
			</td>
		</tr>
		<tr>
			<td style="width: 218px; background-color: rgb(204, 204, 255)">
				<div style="text-align: justify">
					<strong>Размер земельного участка</strong></div>
			</td>
			<td style="width: 218px; background-color: rgb(204, 204, 255)">
				<div style="text-align: justify">
					2447 кв.м. , можно рассмотреть вопрос о покупке доп. земли</div>
			</td>
		</tr>
		<tr>
			<td style="width:218px;">
				<div style="text-align: justify">
					<strong>Строения</strong></div>
			</td>
			<td style="width:407px;">
				<div style="text-align: justify">
					2-х этажное кирпичное здание (общежитие) 451 кв.м. 1982 года постройки, перекрытия крыши выполнены из <a class="special_link" href="//stroi-g.ru/library_1331730994.html" target="_blank" title="Стропильные фермы">ферм и металлоконструкций</a>, состояние удовлетворительное.</div>
			</td>
		</tr>
		<tr>
			<td style="width: 218px; background-color: rgb(204, 204, 255)">
				<div style="text-align: justify">
					<strong>Режим права</strong></div>
			</td>
			<td style="width: 218px; background-color: rgb(204, 204, 255)">
				<div style="text-align: justify">
					Собственность</div>
			</td>
		</tr>
		<tr>
			<td style="width:218px;">
				<div style="text-align: justify">
					<strong>Наличие коммуникаций</strong></div>
			</td>
			<td style="width:407px;">
				<div style="text-align: justify">
					Водоснабжение, канализация, отопление подведены требуется согласование на подключение</div>
			</td>
		</tr>
		<tr>
			<td style="width: 218px; background-color: rgb(204, 204, 255)">
				<div style="text-align: justify">
					<strong>Наличие электричества</strong></div>
			</td>
			<td style="width: 218px; background-color: rgb(204, 204, 255)">
				<div style="text-align: justify">
					Есть</div>
			</td>
		</tr>
		<tr>
			<td style="width: 218px; background-color: rgb(255, 255, 255)">
				<div style="text-align: justify">
					<strong>Готовность документов</strong></div>
			</td>
			<td style="width: 218px; background-color: rgb(255, 255, 255)">
				<div style="text-align: justify">
					Готовы к продаже&nbsp;</div>
			</td>
		</tr>
		<tr>
			<td style="width: 218px; background-color: rgb(204, 204, 255)">
				<div style="text-align: justify">
					<strong>Условия продажи (покупки)</strong></div>
			</td>
			<td style="width: 218px; background-color: rgb(204, 204, 255)">
				<div style="text-align: justify">
					Чистая продажа</div>
			</td>
		</tr>
		<tr>
			<td style="width: 218px; background-color: rgb(255, 255, 255)">
				<div style="text-align: justify">
					<strong>Предполагаемое использование</strong></div>
			</td>
			<td style="width: 218px; background-color: rgb(255, 255, 255)">
				<div style="text-align: justify">
					1. Под гостиницу, базу отдыха (туризм является наиболее перспективным направлением развития района), идеальное место для рыбалки (р. Чеснова - настоящая рыболовная Мекка), охота (в лесу водятся лоси, кабаны, зайцы и медведи)</div>
				<div style="text-align: justify">
					2. Под правительственную программу о строительстве и выделению жилья для пожилых людей. Экологически чистый район, удаленный от промышленных обектов.&nbsp;</div>
			</td>
		</tr>
		<tr>
			<td style="width: 218px; background-color: rgb(204, 204, 255)">
				<div style="text-align: justify">
					<strong>Комплектация</strong></div>
			</td>
			<td style="width: 218px; background-color: rgb(204, 204, 255)">
				<div style="text-align: justify">
					2-х местные &ndash; Люкс с камином &ndash; 2 шт.; 3-х местные; 5-ти местные; Санузел в каждом номере; Столовая. Требуется косметический ремонт здания.</div>
			</td>
		</tr>
		<tr>
			<td style="width: 218px; background-color: rgb(255, 255, 255); text-align: justify">
				<strong>ДОПОЛНИТЕЛЬНО</strong></td>
			<td style="width: 218px; background-color: rgb(255, 255, 255)">
				<p style="text-align: justify">
					1. Возможна реконструкция здания до 3-х этажного плюс мансардный этаж.Есть набросок проекта реконструкции здания с одобрением архитектора г.Ярославля. &nbsp;</p>
				<p style="text-align: justify">
					2. Возможна аренда заброшенного причала сроком до 49 лет для катеров и яхт. Причал расположен в бухте реки Чеснова на выходе в Рыбинское море.&nbsp;</p>
			</td>
		</tr>
	</tbody>
</table>
<p>
	Документация и дополнительная информация предоставляются по запросу заказчика или покупателя.</p>
<p>
	<strong><span style="font-size:14px;"><span style="color:#800000;">Виды здания</span></span></strong></p>
<table border="0" cellpadding="1" cellspacing="1" style="width: 500px">
	<tbody>
		<tr>
			<td>
				<img alt="" src="//990220.ru/images/gallery/upload/1309445145_7.jpg" style="height: 150px; width: 200px; margin-left: 5px; margin-right: 5px; margin-top: 5px; margin-bottom: 5px" /></td>
			<td>
				<img alt="" src="//990220.ru/images/gallery/upload/1309445145_3.jpg" style="height: 150px; width: 200px; margin-left: 5px; margin-right: 5px; margin-top: 5px; margin-bottom: 5px" /></td>
			<td>
				<img alt="" src="//990220.ru/images/gallery/upload/1309445145_4.jpg" style="height: 150px; width: 200px; margin-left: 5px; margin-right: 5px; margin-top: 5px; margin-bottom: 5px" /></td>
			<td>
				<img alt="" src="//990220.ru/images/gallery/upload/1309445145_5.jpg" style="height: 150px; width: 200px; margin-left: 5px; margin-right: 5px; margin-top: 5px; margin-bottom: 5px" /></td>
		</tr>
	</tbody>
</table>
<p>
	<span style="color:#800000;"><strong><span style="font-size:14px;">Окружающая природа</span></strong></span></p>
<table border="0" cellpadding="1" cellspacing="1" style="width: 500px">
	<tbody>
		<tr>
			<td>
				<img alt="" src="//990220.ru/images/gallery/upload/1309445145_8.jpg" style="height: 150px; width: 200px; margin-left: 5px; margin-right: 5px; margin-top: 5px; margin-bottom: 5px" /></td>
			<td>
				<img alt="" src="//990220.ru/images/gallery/upload/1309445145_2.jpg" style="height: 150px; width: 200px; margin-left: 5px; margin-right: 5px; margin-top: 5px; margin-bottom: 5px" /></td>
			<td>
				<img alt="" src="//990220.ru/images/gallery/upload/1309445145_9.jpg" style="height: 150px; width: 200px; margin-left: 5px; margin-right: 5px; margin-top: 5px; margin-bottom: 5px" /></td>
			<td>
				<img alt="" src="//990220.ru/images/gallery/upload/1309445145_6.jpg" style="width: 200px; margin-left: 5px; margin-right: 5px; margin-top: 5px; margin-bottom: 5px" /></td>
		</tr>
		<tr>
			<td style="text-align: center; vertical-align: middle; background-color: rgb(204, 204, 204)">
				<strong>река Чеснова</strong></td>
			<td style="text-align: center; vertical-align: middle; background-color: rgb(204, 204, 204)">
				<strong>Рыбинское море</strong></td>
			<td style="text-align: center; vertical-align: middle; background-color: rgb(204, 204, 204)">
				<strong>Выход в рыбинское море</strong></td>
			<td style="text-align: center; vertical-align: middle; background-color: rgb(204, 204, 204)">
				<strong>Река Чеснова</strong></td>
		</tr>
	</tbody>
</table>
<p>
	<span style="font-size:14px;"><span style="color:#800000;"><strong>Карты расположения обекта относительно г.Москва и г.Ярославль</strong></span></span></p>
<table border="0" cellpadding="1" cellspacing="1" style="width: 500px">
	<tbody>
		<tr>
			<td>
				<img alt="" src="//990220.ru/images/gallery/upload/1309446974_0.jpg" style="height: 225px; width: 300px; margin-left: 5px; margin-right: 5px" /></td>
			<td>
				<img alt="" src="//990220.ru/images/gallery/upload/1309446974_1.jpg" style="height: 225px; width: 300px; margin-left: 5px; margin-right: 5px" /></td>
		</tr>
		<tr>
			<td style="text-align: center; vertical-align: middle; background-color: rgb(204, 204, 204)">
				<strong>удаленность около 200 км от г.Ярославль</strong></td>
			<td style="text-align: center; vertical-align: middle; background-color: rgb(204, 204, 204)">
				<strong>удаленность около 370 км от МКАД г.Москва</strong></td>
		</tr>
	</tbody>
</table>
<p>
	<strong><a href="/simplecontacts.html">КОНТАКТЫ</a></strong></p>
