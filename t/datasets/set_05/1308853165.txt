<h1>
	<span style="color:#800000;">СРЕДСТВА ОБРАБОТКИ ДРЕВЕСИНЫ</span></h1>
<p style="text-align: justify">
	<a href="/pricelist/pricelist_id1311597003.pdf">прайс-лист</a></p>
<p style="text-align: justify">
	Предлагаем Вам высококачественные химические составы и материалы, с помощью которых Вы сможете эффективно бороться с такими вредителями древесины, как грибковые заболевания, плесень, насекомые, ультрафиолетовое излучение.</p>
<p style="text-align: justify">
	Качественная древесина актуальна и востребована на строительном рынке. Это прочный, легкий, экологически чистый материал, который к тому же обладает &laquo;теплой&raquo;&nbsp; структурой&nbsp; и позитивно влияет на здоровье человека.&nbsp; Но, к сожалению, как и любой другой природный материал, древесина не отличается долговечностью. Для того, чтобы продлить жизнь деревянным сооружениям предлагаем следующие средства :&nbsp;</p>
<table border="0" cellpadding="1" cellspacing="1" style="width: 500px">
	<tbody>
		<tr>
			<td>
				<img alt="" src="//990220.ru/images/gallery/upload/1308974488_4.jpg" style="height: 113px; width: 150px; margin-left: 5px; margin-right: 5px; margin-top: 5px; margin-bottom: 5px" /></td>
			<td>
				Отбеливатель для древесины <span style="color:#800000;"><strong>&laquo;Сагус&raquo;</strong></span></td>
		</tr>
		<tr>
			<td style="text-align: center; vertical-align: middle">
				<img alt="" src="//990220.ru/images/gallery/upload/1308974488_2.jpg" style="height: 107px; width: 80px; margin-left: 5px; margin-right: 5px; margin-top: 5px; margin-bottom: 5px" /></td>
			<td>
				Защитно-гидрофобизирующее покрытие <span style="color:#800000;"><strong>&laquo;НОВЬ&raquo;</strong></span></td>
		</tr>
		<tr>
			<td style="text-align: center; vertical-align: middle">
				<img alt="" src="//990220.ru/images/gallery/upload/1308974488_6.jpg" style="height: 107px; width: 80px; margin-left: 5px; margin-right: 5px; margin-top: 5px; margin-bottom: 5px" /></td>
			<td>
				Смывка соли с кирпичной кладки и бетона&nbsp;<span style="color:#800000;"><strong>&laquo;Сагус&raquo;</strong></span></td>
		</tr>
		<tr>
			<td style="text-align: center; vertical-align: middle">
				<img alt="" src="//990220.ru/images/gallery/upload/1308974488_5.jpg" style="height: 107px; width: 80px; margin-left: 5px; margin-right: 5px; margin-top: 5px; margin-bottom: 5px" /></td>
			<td>
				Противоморозная добавка &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;в цементные растворы и бетон &nbsp;<span style="color:#800000;"><strong>&laquo;Сагус&raquo;</strong></span></td>
		</tr>
		<tr>
			<td style="text-align: center; vertical-align: middle">
				<img alt="" src="//990220.ru/images/gallery/upload/1308974488_3.jpg" style="height: 107px; width: 80px; margin-left: 5px; margin-right: 5px; margin-top: 5px; margin-bottom: 5px" /></td>
			<td>
				Огне-биозащита для древесины (второй группы) <span style="color:#800000;"><strong>&laquo;Сагус&raquo;</strong></span></td>
		</tr>
		<tr>
			<td style="text-align: center; vertical-align: middle">
				<img alt="" src="//990220.ru/images/gallery/upload/1308974488_1.jpg" style="height: 107px; width: 80px; margin-left: 5px; margin-right: 5px; margin-top: 5px; margin-bottom: 5px" /></td>
			<td style="width: 500px">
				Грунтовка акриловая универсальная (глубокого проникновения) <span style="color:#800000;"><strong>&laquo;</strong></span><span style="color:#800000;"><strong>Сагус</strong></span><span style="color:#800000;"><strong>&raquo;</strong></span></td>
		</tr>
	</tbody>
</table>
<p style="text-align: justify">
	<span style="color:#800000;"><strong>&laquo;Сагус&raquo;</strong></span> -&nbsp; лучший на сегодняшний день отбеливатель древесины. Его хлорсодержащая составляющая на водной основе эффективно уничтожает грибковые образования и плесень, поражающие древесину, не разрушая при этом ее структуру. Отбеливатель для древесины <span style="color:#800000;"><strong>&laquo;Сагус&raquo;</strong></span> так же можно использовать в реставрационных целях,&nbsp; для того чтобы старой, посеревшей и почерневшей древесине возвратить ее естественный цвет и красоту.</p>
<p style="text-align: justify">
	Мы являемся фирмой-официальным дилером компании это позволяет нам проводить гибкую ценовую политику, что способствует конкурентоспособности нашей продукции. Поэтому покупая огнебиозащиту или отбеливатель древесины &laquo;Сагус&raquo; от производителя, Вы можете быть уверены в том, что приобретаете высокое качество по доступной цене.</p>
<p style="text-align: justify">
	Деревянные дома сегодня - это просто и изыскано, красиво и полезно для здоровья. Это нравится всем. Однако не все помнят о том, что красивая древесина может быстро потерять свой товарный вид, если ее вовремя не обработать антисептиком. Поэтому для поддержания естественной красоты обязательно купите &laquo;Сагус&raquo;, ведь это лучший отбеливатель древесины от производителя. Ни один материал не может сравниться с красотой натурального дерева.&nbsp;</p>
<h3 style="color: #ff0000; text-align: justify">
	ВНИМАНИЕ!</h3>
<p style="text-align: justify">
	В связи с участившимися случаями подделки данного товара, в частности отбеливателя для древесины, просим Вас обратить внимание на товар, который Вы приобреаете! На этикетке нанесено голографическое изображение, которое имеет свой индивидуальный номер.</p>
<p style="text-align: justify">
	<img alt="" src="//990220.ru/images/gallery/upload/1308974488_0.jpg" style="height: 133px; width: 133px" /></p>
<p style="text-align: justify">
	Фирма <span style="color:#800000;"><strong>&laquo;Сагус&raquo;</strong></span> является единственным производителем отбеливателя для древесины марки <span style="color:#800000;"><strong>&laquo;Сагус&raquo;</strong></span>! Если Вы стали жертвой обмана и приобрели поддельный товар &ndash; убедительная просьба немедленно связаться с нашими сотрудниками. Так же Вы можете написать нам на электронный адрес. или на Skype (см. раздел &quot;<a href="//
simplecontacts.html">Контакты</a>&quot;).</p>
