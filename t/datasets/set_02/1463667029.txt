<div class="head-delimiter"></div>

<div class="content indent">
    <!--content-->
    <div class="thumb-box9" style="margin-top:-30px;">
        <div class="container">
            <p class="title"><span>Объекты</span></p>
	    <p class="object-item-header">Щепкина, 42</p>
	    <p class="object-item-descr">РОСКОСМОС – государственная корпорация, созданная в августе 2015 года для проведения комплексной реформы ракетно-космической отрасли России.<br>
        Госкорпорация "РОСКОСМОС" обеспечивает реализацию госполитики в области космической деятельности и ее нормативно-правовое регулирование, а также размещает заказы на разработку, производство и поставку космической техники и объектов космической инфраструктуры.<br>
        В функции государственной корпорации также входит развитие международного сотрудничества в космической сфере и создание условий для использования результатов космической деятельности в социально-экономическом развитии России.</p>
        </div>
    </div>
<div class="thumb-box2">
        <div class="container">
            <p class="title wow fadeInUp"><span style="font-size:16pt;">Фотографии</span></p>
            <div class="row wow fadeInUp">
                <div class="col-lg-8 col-md-10 col-lg-offset-2 col-md-offset-1">
                    <div class="list_carousel1 responsive clearfix ">
                        <ul id="foo1">
                            <li>
                                <figure><img src="images/castiglion/objects/3/pic-1.jpg" alt=""></figure>
                            </li>
                            <li>
                                <figure><img src="images/castiglion/objects/3/pic-2.jpg" alt=""></figure>
                            </li>
                            <li>
                                <figure><img src="images/castiglion/objects/3/pic-3.jpg" alt=""></figure>
                            </li>
                            <li>
                                <figure><img src="images/castiglion/objects/3/pic-4.jpg" alt=""></figure>
                            </li>
                            <li>
                                <figure><img src="images/castiglion/objects/3/pic-5.jpg" alt=""></figure>
                            </li>
                        </ul>
                    </div>
                    <div class="foo-btn clearfix">
                        <div class="pagination" id="foo2_pag"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<section class="content_map">
      <div class="google-map-api"> 
        <div id="map-canvas" class="gmap"></div> 
      </div> 
</section>