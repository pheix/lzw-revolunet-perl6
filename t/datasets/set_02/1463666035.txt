<div class="head-delimiter"></div>

<div class="content indent">
    <!--content-->

<div class="thumb-box9" style="margin-top:-30px;">
        <div class="container">
            <p class="title"><span>Новости</span></p>
            <div class="row">
<div class="col-lg-6 col-md-6 col-sm-6">
                    <h3 class="news-header-h2">Новая стройплощадка</h3>
                    <h4>На площадке сооружения новых блоков Нововоронежской АЭС-2 состоялось торжественное открытие Зональной студенческой стройки.</h4>
                    <p>На торжественной церемонии открытия студенческой стройки присутствовали специально приглашенные гости: доверенное лицо президента РФ Вячеслав Шамарин – председатель правления Воронежского регионального отделения молодежной общественной организации «Российские студенческие отряды»; Геннадий Громяцкий – заместитель руководителя аппарата Центрального штаба «Российские студенческие отряды»; Максим Богачко – директор филиала НФ-ДС «АО «Атомэнергопроект»; Николай Петренко – директор по управлению собственными силами АО «НИАЭП».</p>

<p>По доброй стройотрядовской традиции, каждый отряд получил «Путевку». Путевки командирам отрядов вручил Виталий Полянин – вице-президент, руководитель проекта сооружения Нововоронежской АЭС АО «НИАЭП». В своей приветственной речи Виталий Олегович рассказал, что его студенческие годы выпали на рассвет развития ССО, и ровно 30 лет назад он с перрона Ленинградского вокзала в составе стройотряда «Атлант» уезжал в Коми АССР. Полянин признался, что в первое лето он не заработал много денег, но самое главное, что он привез с «целины» – это новые друзья, гордость за построенные в Сыктывкаре, Ухте и Воркуте объекты, а также умение работать в команде.</p>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6" data-wow-delay="0.1s">
                    <h3 class="news-header-h2">Фотографии с ЦПК</h3>
                    <figure><a href="object-1.html"><img src="images/castiglion/news/cpk.jpg" alt="" style="margin-bottom:20px;"></a></figure>
                    <p class="description">Федеральное государственное бюджетное учреждение "Научно-исследовательский испытательный центр подготовки космонавтов имени Ю.А.Гагарина" создано для проведения работ по обеспечению пилотируемых космических программ, научно-исследовательских и опытно-конструкторских работ в области изучения космического пространства и создания космической техники, подготовки космонавтов, обеспечения безопасного пребывания космонавтов на орбите, реабилитации космонавтов после выполнения космических полетов.</p>
                </div>                          
            </div>
        </div>
    </div>
    <!--content-->  
</div>

<section class="content_map">
      <div class="google-map-api"> 
        <div id="map-canvas" class="gmap"></div> 
      </div> 
</section>