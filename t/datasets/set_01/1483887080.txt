<table align="left" border="0" cellpadding="0" cellspacing="0" style="width:770px">
	<tbody>
		<tr>
		</tr>
		<tr>
			<td colspan="2" rowspan="2">
			<h1>Зимнее Вело ПВД Морозки-Трудовая (6 декабря 2014 г.)</h1>

			<p>&nbsp;</p>

			<hr />
			<hr />
			<p><br />
			Морозки - Пенсионер - Хорошилово - Лупаново - Кузяево - Малая Черная - Микрорайон Строителей - Трудовая. Всего 31 км.</p>

			<p>Участники: Павел Ляхов, Никита Чебаков, Максим К.</p>

			<p>Трэк:&nbsp;<a href="http://www.gpsies.com/map.do?fileId=uzzbgzhdwcopenid"><img alt="GPSies - Морозки-Трудовая" src="http://www.gpsies.com/images/linkus.png" /></a></p>

			<p>&nbsp;</p>

			<p>Минимум асфальта и хорошо промороженные грунтовые дороги с неглубоким снегом (около 5 см.) сделали дорогу приятной и не особо сложной. На участке от Морозок до СНТ Пенсионер местами дорога была разбита квадроциклами, но разбитые участки были удачно &quot;заасфальтированы&quot; льдом.</p>

			<p>На выезде из Хорошилово, в лесу тропа тоже сильно разбита квадроциклами, часть пути даже прошли пешком. По многим просекам и тропам видно что была проведена подготовка лыжней, произведены пропилы, так что в целом дороги везде хорошие.</p>

			<p><br />
			На трудовую приехали уже по темноте: во тьме ехали от самого Лупанова -издержки малого светового дня зимой. Учитывая малую глубину снега (в хвойных лесах его вообще не было), на лыжах там пока делать нечего, и выбор велосипедов в качестве транспортного средства себя оправдал в полной мере.</p>

			<p><img src="http://drojj-taigi.narod.ru/pvd-mor-trud-2014/dsc_8185.jpg" style="height:534px; width:800px" /></p>

			<p>&nbsp;</p>

			<p><img src="http://drojj-taigi.narod.ru/pvd-mor-trud-2014/pict0001.jpg" style="height:532px; width:800px" /></p>

			<p>&nbsp;</p>

			<p><img src="http://drojj-taigi.narod.ru/pvd-mor-trud-2014/pict0007.jpg" style="height:532px; width:800px" /></p>

			<p><img src="http://drojj-taigi.narod.ru/pvd-mor-trud-2014/dsc_8191.jpg" style="height:534px; width:800px" /></p>

			<p><img src="http://drojj-taigi.narod.ru/pvd-mor-trud-2014/dsc_8202.jpg" style="height:534px; width:800px" /></p>

			<p>&nbsp;</p>

			<p><img src="http://drojj-taigi.narod.ru/pvd-mor-trud-2014/pict0026.jpg" style="height:532px; width:800px" /></p>

			<p><img src="http://drojj-taigi.narod.ru/pvd-mor-trud-2014/pict0043.jpg" style="height:532px; width:800px" /></p>

			<p><img src="http://drojj-taigi.narod.ru/pvd-mor-trud-2014/pict0028.jpg" style="height:532px; width:800px" /></p>

			<p>&nbsp;</p>

			<p><img src="http://drojj-taigi.narod.ru/pvd-mor-trud-2014/dsc_8237.jpg" style="height:534px; width:800px" /></p>

			<p>&nbsp;&nbsp;<var>05.12.2014. Павел Ляхов</var></p>

			<p>&nbsp;</p>
			</td>
			<td>
			<table border="0" style="width:200px">
				<tbody>
					<tr>
						<td>&nbsp;</td>
					</tr>
				</tbody>
			</table>
			</td>
		</tr>
		<tr>
			<td>&nbsp;<br />
			&nbsp;</td>
		</tr>
	</tbody>
</table>
