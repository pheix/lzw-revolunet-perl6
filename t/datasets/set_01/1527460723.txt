<h1>Конаково - Тверь на фэтбайке за один день. 26.05.2018 (около 90 км)</h1>

<p>По классическому маршруту прокатился в первый раз. Почти первый, потому что в прошлом году я проезжал его начать по дороге в Оршинские болота.<br />
Маршрут очень понравился благодаря большому количеству грунтовых дорог и красивых видов. Этот маршрут не зря проходят за два дня - по такой красоте надо ехать медленно, наслаждаясь видами и вдыхая полезный сосново-хвойный воздух. В июле есть смысл туда съездить - отведать местной черники, так как почти все сосновые леса, раскинувшиеся вдоль дорожек окутаны ярко-зеленым ковром черничника. По маршруту в деревнях встречаются магазины (в Заборовье работал точно - закупился там на обед), но вернее закупиться в Конаково. Паром от Конаково ходит раз в час кратно 45 минутам: 10:45, 11:45 и так далее.<br />
Интерес представляют песчаные дороги - находка для фэтбайкера :-), багульниковые заросли, поросшие соснами песчаные дюны, песчаный карьер с узкоколейной железной дорогой и др. Есть брод, переходимый в мае ровно по пояс. Летом брод обмельчает.<br />
<br />
<strong>Видео</strong></p>

<p><br />
<iframe frameborder="0" height="400" name="embed_84211443_11" src="https://l.lj-toys.com/?auth_token=sessionless%3A1527458400%3Aembedcontent%3A84211443%2611%26%26%26youtube%26_ChTH6772Dk%3A58a9ffcf6f45e712776796b6695d38d871767d50&amp;source=youtube&amp;vid=_ChTH6772Dk&amp;moduleid=11&amp;preview=&amp;journalid=84211443&amp;noads=" width="800"></iframe><br />
<br />
<strong>Несколько фото с маршрута:</strong><br />
<a href="https://ic.pics.livejournal.com/drtg3/84211443/18273/18273_original.jpg" target="_blank"><img alt="" src="https://ic.pics.livejournal.com/drtg3/84211443/18273/18273_800.jpg" /></a><br />
Познакомился в электричке с Артемом с Веломании. Душевно пообщались, попили пивка пока ждали паром.<br />
<br />
<br />
<a href="https://ic.pics.livejournal.com/drtg3/84211443/18904/18904_original.jpg" target="_blank"><img alt="" src="https://ic.pics.livejournal.com/drtg3/84211443/18904/18904_800.jpg" /></a><br />
Корпуса заброшенной базы отдыха около деревни Едимоново.<br />
<br />
<a href="https://ic.pics.livejournal.com/drtg3/84211443/19032/19032_original.jpg" target="_blank"><img alt="" src="https://ic.pics.livejournal.com/drtg3/84211443/19032/19032_800.jpg" /></a><br />
Песчанные дюны, поросшие сосновым бором.<br />
<br />
<a href="https://ic.pics.livejournal.com/drtg3/84211443/19405/19405_original.jpg" target="_blank"><img alt="" src="https://ic.pics.livejournal.com/drtg3/84211443/19405/19405_800.jpg" /></a><br />
Багульник цветет.<br />
<br />
<a href="https://ic.pics.livejournal.com/drtg3/84211443/19576/19576_original.jpg" target="_blank"><img alt="" src="https://ic.pics.livejournal.com/drtg3/84211443/19576/19576_800.jpg" /></a><br />
Брод пройден!<br />
<br />
<a href="https://ic.pics.livejournal.com/drtg3/84211443/19857/19857_original.jpg" target="_blank"><img alt="" src="https://ic.pics.livejournal.com/drtg3/84211443/19857/19857_800.jpg" /></a><br />
Магия сосен.<br />
<br />
<a href="https://ic.pics.livejournal.com/drtg3/84211443/20196/20196_original.jpg" target="_blank"><img alt="" src="https://ic.pics.livejournal.com/drtg3/84211443/20196/20196_800.jpg" /></a><br />
Магия цветов.<br />
<br />
<a href="https://ic.pics.livejournal.com/drtg3/84211443/20545/20545_original.jpg" target="_blank"><img alt="" src="https://ic.pics.livejournal.com/drtg3/84211443/20545/20545_800.jpg" /></a><br />
Чеканка в Лисицах.<br />
<br />
<a href="https://ic.pics.livejournal.com/drtg3/84211443/20245/20245_original.jpg" target="_blank"><img alt="" src="https://ic.pics.livejournal.com/drtg3/84211443/20245/20245_800.jpg" /></a><br />
Украшенная чеканкой пристань в Лисицах.<br />
<br />
<br />
<a href="https://ic.pics.livejournal.com/drtg3/84211443/20829/20829_original.jpg" target="_blank"><img alt="" src="https://ic.pics.livejournal.com/drtg3/84211443/20829/20829_800.jpg" /></a><br />
Обед&nbsp;<s>спортсмена</s>&nbsp;физкультурника.<br />
<br />
<a href="https://ic.pics.livejournal.com/drtg3/84211443/21190/21190_original.jpg" target="_blank"><img alt="" src="https://ic.pics.livejournal.com/drtg3/84211443/21190/21190_800.jpg" /></a><br />
Мост через Оршу в Савватьево.<br />
<br />
<a href="https://ic.pics.livejournal.com/drtg3/84211443/21303/21303_original.jpg" target="_blank"><img alt="" src="https://ic.pics.livejournal.com/drtg3/84211443/21303/21303_800.jpg" /></a><br />
Церковь в Савватьево.<br />
<br />
<br />
<a href="https://ic.pics.livejournal.com/drtg3/84211443/21540/21540_original.jpg" target="_blank"><img alt="" src="https://ic.pics.livejournal.com/drtg3/84211443/21540/21540_800.jpg" /></a><br />
Чарничные ковры.<br />
<br />
<a href="https://ic.pics.livejournal.com/drtg3/84211443/21931/21931_original.jpg" target="_blank"><img alt="" src="https://ic.pics.livejournal.com/drtg3/84211443/21931/21931_800.jpg" /></a><br />
Детский поезд.<br />
<br />
<a href="https://ic.pics.livejournal.com/drtg3/84211443/22209/22209_original.jpg" target="_blank"><img alt="" src="https://ic.pics.livejournal.com/drtg3/84211443/22209/22209_800.jpg" /></a><br />
Гранд Каньон под Тверью.<br />
<br />
<br />
<a href="https://ic.pics.livejournal.com/drtg3/84211443/22437/22437_original.jpg" target="_blank"><img alt="" src="https://ic.pics.livejournal.com/drtg3/84211443/22437/22437_800.jpg" /></a><br />
Кладбище паровозов.<br />
<br />
<a href="https://ic.pics.livejournal.com/drtg3/84211443/22672/22672_original.jpg" target="_blank"><img alt="" src="https://ic.pics.livejournal.com/drtg3/84211443/22672/22672_800.jpg" /></a><br />
<br />
<a href="https://ic.pics.livejournal.com/drtg3/84211443/23021/23021_original.jpg" target="_blank"><img alt="" src="https://ic.pics.livejournal.com/drtg3/84211443/23021/23021_800.jpg" /></a><br />
<br />
<a href="https://ic.pics.livejournal.com/drtg3/84211443/23282/23282_original.jpg" target="_blank"><img alt="" src="https://ic.pics.livejournal.com/drtg3/84211443/23282/23282_800.jpg" /></a><br />
<br />
<a href="https://ic.pics.livejournal.com/drtg3/84211443/23397/23397_original.jpg" target="_blank"><img alt="" src="https://ic.pics.livejournal.com/drtg3/84211443/23397/23397_800.jpg" style="height:600px; width:800px" /></a><br />
<br />
<a href="https://ic.pics.livejournal.com/drtg3/84211443/23644/23644_original.jpg" target="_blank"><img alt="" src="https://ic.pics.livejournal.com/drtg3/84211443/23644/23644_800.jpg" /></a><br />
Тут можно и искупаться. Песок, вода прозрачная!</p>

<h2><strong>Трэк маршрута Конаково-Тверь 2018</strong></h2>

<p><strong>Трэк в формате gpx:</strong>&nbsp;<a href="https://yadi.sk/d/ECOj3aDl3Wbdw9" target="_blank">https://yadi.sk/d/ECOj3aDl3Wbdw9</a></p>

<p>&nbsp;</p>

<p><br />
<br />
<br />
<iframe frameborder="0" height="500" name="embed_84211443_10" src="https://l.lj-toys.com/?auth_token=sessionless%3A1527458400%3Aembedcontent%3A84211443%2610%26%26%3A503af1bb582f74c4319d1e9de14b7a0e425bddf1&amp;moduleid=10&amp;preview=&amp;journalid=84211443&amp;noads=" width="800"></iframe></p>

<p><strong>МЕТКИ:&nbsp;</strong><a href="https://drtg3.livejournal.com/tag/%D0%B2%D0%B5%D0%BB%D0%BE%D0%BF%D0%B2%D0%B4" target="_self">велопвд</a>,&nbsp;<a href="https://drtg3.livejournal.com/tag/%D1%84%D1%8D%D1%82%D0%B1%D0%B0%D0%B9%D0%BA" target="_self">фэтбайк</a></p>
