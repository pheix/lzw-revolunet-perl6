<h1>Вело ПВД 100 км за один день Конаково-Клин ( 3 июня 2014 г.)</h1>

<p><br />
Конаково - Клин. Всего 100 км.</p>

<p><strong>Участники:</strong> Павел Ляхов, Никита Чебаков, Слава.</p>

<p><strong>Трэк:</strong>&nbsp;<a href="http://www.gpsies.com/map.do?fileId=llrjcxumqwdihsko"><img alt="GPSies - Северная весна" src="http://www.gpsies.com/images/linkus.png" /></a></p>

<p>Нашел интересный маршрут на сайте gpsies &quot;Северная весна&quot; от Syrinx. У меня было давнее желание пробороздить на своем байке просторы того района. Решили проехать маршрут за один день. В идеале разбить маршрут на два дня, как собственно рекомендовали авторы, составившие его. Думаю если будет возможность я проеду там еще раз но уже с ночевкой.<br />
Погода была отличная, даже жарко. В целом трэк отличный -&nbsp; везде все хорошо проезжаемо, опасных участков почти нет. Наша версия трэка:&nbsp;<a href="http://www.gpsies.com/map.do?fileId=fyfcvswsnjhznrql">http://www.gpsies.com/map.do?fileId=fyfcvswsnjhznrql</a></p>

<p><img src="http://drojj-taigi.narod.ru/konakovo-klin2014/k001.jpg" style="height:600px; width:800px" /><em>На платформе Конаково ГРЭС</em></p>

<p><img src="http://drojj-taigi.narod.ru/konakovo-klin2014/k002.jpg" style="height:600px; width:800px" /></p>

<p><img src="http://drojj-taigi.narod.ru/konakovo-klin2014/k003.jpg" style="width:800px" /><em>Памятник Порфирию Петровичу Конакову - рабочему фаянцового завода! Не толькл унитазы делать, товагищь! Даешь, батенька Геволюцию!!!</em></p>

<p><img src="http://drojj-taigi.narod.ru/konakovo-klin2014/k004.jpg" style="height:600px; width:800px" /><em>Трубы Конаковской ГРЭС</em></p>

<p><img src="http://drojj-taigi.narod.ru/konakovo-klin2014/k005.jpg" style="width:800px" />Опоры ЛЭП Конаковской ГРЭС</p>

<p><img src="http://drojj-taigi.narod.ru/konakovo-klin2014/k006.jpg" style="height:600px; width:800px" /><em>Дорога между Конаково и Федоровское</em></p>

<p><img src="http://drojj-taigi.narod.ru/konakovo-klin2014/k007.jpg" style="height:600px; width:800px" /></p>

<p><img src="http://drojj-taigi.narod.ru/konakovo-klin2014/k008.jpg" style="height:600px; width:800px" /></p>

<p><img src="http://drojj-taigi.narod.ru/konakovo-klin2014/k009.jpg" style="width:800px" /><em>Ну да, типа не жги лес, и берегии, короче его...</em></p>

<p><img src="http://drojj-taigi.narod.ru/konakovo-klin2014/k010.jpg" style="height:600px; width:800px" /></p>

<p><img src="http://drojj-taigi.narod.ru/konakovo-klin2014/k011.jpg" style="height:600px; width:800px" /><em>Грязевой участок на дороге между Конаково и Федоровское</em></p>

<p><img src="http://drojj-taigi.narod.ru/konakovo-klin2014/k012.jpg" style="height:600px; width:800px" /><em>Замок на оградке заброшенного кладбища</em></p>

<p><img src="http://drojj-taigi.narod.ru/konakovo-klin2014/k013.jpg" style="height:600px; width:800px" /><em>Заброшенная церковь в деревне Федоровское</em></p>

<p><img src="http://drojj-taigi.narod.ru/konakovo-klin2014/k014.jpg" style="height:600px; width:800px" /><em>Заброшенная церковь в деревне Федоровское</em></p>

<p><img src="http://drojj-taigi.narod.ru/konakovo-klin2014/k015.jpg" style="height:600px; width:800px" /><em>Заброшенная церковь в деревне Федоровское</em></p>

<p><img src="http://drojj-taigi.narod.ru/konakovo-klin2014/k016.jpg" style="width:800px" /><em>Заброшенная церковь в деревне Федоровское</em></p>

<p><img src="http://drojj-taigi.narod.ru/konakovo-klin2014/k017.jpg" style="height:600px; width:800px" /><em>Заброшенная церковь в деревне Федоровское</em></p>

<p><img src="http://drojj-taigi.narod.ru/konakovo-klin2014/k018.jpg" style="height:600px; width:800px" /><em>Заброшенная церковь в деревне Федоровское</em>.<em>&nbsp;Интерьеры.</em></p>

<p><img src="http://drojj-taigi.narod.ru/konakovo-klin2014/k019.jpg" style="height:600px; width:800px" /><em>Заброшенная церковь в деревне Федоровское</em>.&nbsp;<em>Свод.</em></p>

<p><img src="http://drojj-taigi.narod.ru/konakovo-klin2014/k020.jpg" style="height:600px; width:800px" /><em>Заброшенная церковь в деревне Федоровское</em>.&nbsp;<em>Росписи</em></p>

<p><img src="http://drojj-taigi.narod.ru/konakovo-klin2014/k021.jpg" style="width:800px" /><em>Заброшенная церковь в деревне Федоровское. Росписи</em></p>

<p><img src="http://drojj-taigi.narod.ru/konakovo-klin2014/k022.jpg" style="height:600px; width:800px" /><em>Заброшенная церковь в деревне Федоровское. Мертвая птица</em></p>

<p><img src="http://drojj-taigi.narod.ru/konakovo-klin2014/k023.jpg" style="height:600px; width:800px" /><em>Заброшенная церковь в деревне Федоровское</em></p>

<p><img src="http://drojj-taigi.narod.ru/konakovo-klin2014/k024.jpg" style="height:600px; width:800px" /><em>Заброшенная церковь в деревне Федоровское</em></p>

<p><img src="http://drojj-taigi.narod.ru/konakovo-klin2014/k025.jpg" style="height:600px; width:800px" /><em>Заброшенная церковь в деревне Федоровское</em></p>

<p><img src="http://drojj-taigi.narod.ru/konakovo-klin2014/k026.jpg" style="height:600px; width:800px" /><em>Грейдер от Федоровского до деревни Юрьево</em></p>

<p><img src="http://drojj-taigi.narod.ru/konakovo-klin2014/k027.jpg" style="height:600px; width:800px" /><em>Руины церкви в Трехсвятском</em></p>

<p><img src="http://drojj-taigi.narod.ru/konakovo-klin2014/k027-1.jpg" style="height:600px; width:800px" /><em>Руины церкви в Трехсвятском</em></p>

<p><img src="http://drojj-taigi.narod.ru/konakovo-klin2014/k028.jpg" style="height:600px; width:800px" /><em>Дорога от Трехсвятского до Атеевки</em></p>

<p><img src="http://drojj-taigi.narod.ru/konakovo-klin2014/k029.jpg" style="height:600px; width:800px" /><em>Мост через реку Сестра. Мост оказался недостроенным, и радость от встречи с ним была преждевременной. По трубе не пошли - пошли вброд.</em></p>

<p><img src="http://drojj-taigi.narod.ru/konakovo-klin2014/k030.jpg" style="height:600px; width:800px" /></p>

<p><img src="http://drojj-taigi.narod.ru/konakovo-klin2014/k031.jpg" style="height:600px; width:800px" /><em>На деревянной платформе (как в 19 веке :-)) на станции Клин. Доехали только мы с Никитой. Слава сошел с маршрута в деревне Слобода</em></p>

<p><img src="http://drojj-taigi.narod.ru/konakovo-klin2014/k032.jpg" style="height:600px; width:800px" /><em>В электричке - довольные едем домой!!!</em></p>
