<h1>Поездка<strong>&nbsp;в Нижний Новгород с 26 - 28 сентября 2018</strong></h1>

<p><br />
<br />
Оказался я в Горьком &nbsp;[Нижнем Новгороде] пребывая в командировке на 7-ю ежегодную научно-практическую конференцию &laquo;Ассоциации специалистов по лабораторным животным&raquo; (Rus-LASA), которая&nbsp; проходила в Нижегородском государственном университете им. Н.И. Лобачевского .<br />
<br />
Я приехал на день раньше, так как мне необходимо было оборудовать стенд и подготовить наше выставочное место к завтрашнему событию и в связи с тем, что рано освободился, у меня было еще около 4-х часов светлого дневного времени этого дня и еще два вечера - вполне достаточно времени для поверхностного исследования города.<br />
На первый день я запланировал посещение Нижегородского Кремля и конечно канатной дороги через Волгу.<br />
<br />
<a href="https://ic.pics.livejournal.com/drtg6/84837712/295/295_original.jpg" target="_blank"><img alt="" src="https://ic.pics.livejournal.com/drtg6/84837712/295/295_800.jpg" /></a>&nbsp;<br />
<br />
<a href="https://ic.pics.livejournal.com/drtg6/84837712/641/641_original.jpg" target="_blank"><img alt="" src="https://ic.pics.livejournal.com/drtg6/84837712/641/641_800.jpg" /></a></p>

<p><br />
<strong>Нижегородское Метро</strong><br />
Прокатился и на метро! Был только на трех станциях Московская, Горьковская и автозаводская. Интерьеры не хуже чем в Московском метро. Станции по длине перрона примерно в два раза короче, длинна состава - четыре вагона, интервал между поездами в 11:50 был 6 с половиной минут, стоимость проезда 28 руб.&nbsp;Для прохода в Метро используются металлические жетоны (похожие были в Москве в 90-х годах).<br />
<br />
<a href="https://ic.pics.livejournal.com/drtg6/84837712/864/864_original.jpg" target="_blank"><img alt="" src="https://ic.pics.livejournal.com/drtg6/84837712/864/864_800.jpg" /></a><br />
<br />
<a href="https://ic.pics.livejournal.com/drtg6/84837712/1227/1227_original.jpg" target="_blank"><img alt="" src="https://ic.pics.livejournal.com/drtg6/84837712/1227/1227_800.jpg" /></a><br />
<br />
<a href="https://ic.pics.livejournal.com/drtg6/84837712/1447/1447_original.jpg" target="_blank"><img alt="" src="https://ic.pics.livejournal.com/drtg6/84837712/1447/1447_800.jpg" /></a><br />
<br />
<a href="https://ic.pics.livejournal.com/drtg6/84837712/1604/1604_original.jpg" target="_blank"><img alt="" src="https://ic.pics.livejournal.com/drtg6/84837712/1604/1604_800.jpg" /></a><br />
<br />
<a href="https://ic.pics.livejournal.com/drtg6/84837712/1812/1812_original.jpg" target="_blank"><img alt="" src="https://ic.pics.livejournal.com/drtg6/84837712/1812/1812_800.jpg" /></a><br />
<br />
<strong>Нижегородский Кремль</strong><br />
Меня поразила насыпь, и рвы, по которым сейчас проложены дороги вокруг Кремля. На такой насыпи сложно удержаться на ногах. По мокрой траве ноги скользят и есть риск скатиться и упасть на проезжую полосу.<br />
<br />
<br />
<a href="https://ic.pics.livejournal.com/drtg6/84837712/2222/2222_original.jpg" target="_blank"><img alt="" src="https://ic.pics.livejournal.com/drtg6/84837712/2222/2222_800.jpg" /></a><br />
<br />
<a href="https://ic.pics.livejournal.com/drtg6/84837712/2397/2397_original.jpg" target="_blank"><img alt="" src="https://ic.pics.livejournal.com/drtg6/84837712/2397/2397_800.jpg" /></a><br />
<br />
<a href="https://ic.pics.livejournal.com/drtg6/84837712/2699/2699_original.jpg" target="_blank"><img alt="" src="https://ic.pics.livejournal.com/drtg6/84837712/2699/2699_800.jpg" /></a><br />
<br />
<a href="https://ic.pics.livejournal.com/drtg6/84837712/2922/2922_original.jpg" target="_blank"><img alt="" src="https://ic.pics.livejournal.com/drtg6/84837712/2922/2922_800.jpg" /></a><br />
<br />
<a href="https://ic.pics.livejournal.com/drtg6/84837712/3280/3280_original.jpg" target="_blank"><img alt="" src="https://ic.pics.livejournal.com/drtg6/84837712/3280/3280_800.jpg" /></a><br />
<br />
<a href="https://ic.pics.livejournal.com/drtg6/84837712/3571/3571_original.jpg" target="_blank"><img alt="" src="https://ic.pics.livejournal.com/drtg6/84837712/3571/3571_800.jpg" /></a><br />
<em>Дом правительства Нижегородской Области.</em><br />
<br />
<a href="https://ic.pics.livejournal.com/drtg6/84837712/3672/3672_original.jpg" target="_blank"><img alt="" src="https://ic.pics.livejournal.com/drtg6/84837712/3672/3672_800.jpg" /></a><br />
<br />
<a href="https://ic.pics.livejournal.com/drtg6/84837712/4046/4046_original.jpg" target="_blank"><img alt="" src="https://ic.pics.livejournal.com/drtg6/84837712/4046/4046_800.jpg" /></a><br />
<br />
<a href="https://ic.pics.livejournal.com/drtg6/84837712/4197/4197_original.jpg" target="_blank"><img alt="" src="https://ic.pics.livejournal.com/drtg6/84837712/4197/4197_800.jpg" /></a><br />
<br />
<a href="https://ic.pics.livejournal.com/drtg6/84837712/4374/4374_original.jpg" target="_blank"><img alt="" src="https://ic.pics.livejournal.com/drtg6/84837712/4374/4374_800.jpg" /></a><br />
<br />
<a href="https://ic.pics.livejournal.com/drtg6/84837712/4637/4637_original.jpg" target="_blank"><img alt="" src="https://ic.pics.livejournal.com/drtg6/84837712/4637/4637_800.jpg" /></a><br />
<br />
<a href="https://ic.pics.livejournal.com/drtg6/84837712/4925/4925_original.jpg" target="_blank"><img alt="" src="https://ic.pics.livejournal.com/drtg6/84837712/4925/4925_800.jpg" /></a><br />
<br />
<strong>Выставка военной техники в Нижегородском Кремле</strong><br />
На территории архитектурного комплекса вдоль крепостных стен располагается небольшая выставка военной техники, которую производили на заводе ГАЗ, Красное Сормово и других Горьковских предприятиях.<br />
<br />
<br />
<a href="https://ic.pics.livejournal.com/drtg6/84837712/5309/5309_original.jpg" target="_blank"><img alt="" src="https://ic.pics.livejournal.com/drtg6/84837712/5309/5309_800.jpg" /></a><br />
<br />
<a href="https://ic.pics.livejournal.com/drtg6/84837712/5485/5485_original.jpg" target="_blank"><img alt="" src="https://ic.pics.livejournal.com/drtg6/84837712/5485/5485_800.jpg" /></a><br />
<br />
<a href="https://ic.pics.livejournal.com/drtg6/84837712/5780/5780_original.jpg" target="_blank"><img alt="" src="https://ic.pics.livejournal.com/drtg6/84837712/5780/5780_800.jpg" /></a><br />
<br />
<a href="https://ic.pics.livejournal.com/drtg6/84837712/5931/5931_original.jpg" target="_blank"><img alt="" src="https://ic.pics.livejournal.com/drtg6/84837712/5931/5931_800.jpg" /></a><br />
<br />
<a href="https://ic.pics.livejournal.com/drtg6/84837712/6360/6360_original.jpg" target="_blank"><img alt="" src="https://ic.pics.livejournal.com/drtg6/84837712/6360/6360_800.jpg" /></a><br />
<br />
<a href="https://ic.pics.livejournal.com/drtg6/84837712/6594/6594_original.jpg" target="_blank"><img alt="" src="https://ic.pics.livejournal.com/drtg6/84837712/6594/6594_800.jpg" /></a><br />
Подводная лодка С-13 была заложена 19 октября 1938 года в Горьком на заводе № 112 &laquo;Красное Сормово&raquo;, спущена на воду 25 апреля 1939 года, вступила в строй 31 июля 1941 года и 14 августа 1941 года вошла в состав Балтийского флота. Единственная подлодка серии &laquo;С&raquo;, дожившая до Победы на Балтике.<br />
<br />
<a href="https://ic.pics.livejournal.com/drtg6/84837712/6785/6785_original.jpg" target="_blank"><img alt="" src="https://ic.pics.livejournal.com/drtg6/84837712/6785/6785_800.jpg" /></a><br />
<br />
<br />
<a href="https://ic.pics.livejournal.com/drtg6/84837712/6929/6929_original.jpg" target="_blank"><img alt="" src="https://ic.pics.livejournal.com/drtg6/84837712/6929/6929_800.jpg" /></a></p>

<p><em>Памятник Валерию Чкалову &mdash; одна из достопримечательностей Нижнего Новгорода, установлен вблизи Георгиевской башни Нижегородского кремля в честь знаменитого советского лётчика, совершившего первый беспосадочный перелёт из СССР в США через Северный полюс. От памятника к Волге спускается лестница, которую также называют &laquo;Чкаловской&raquo;.</em></p>

<p><em>На месте современного памятника В. П. Чкалову в начале XVII века был основан Происхожденский женский монастырь, впоследствии получивший название Крестовоздвиженского и перенесённый в 1815 году на своё нынешнее место у площади Лядова (фото будут ниже). Ветхие строения монастыря сохранялись до 1840 года, когда сносились все строения на прилегающих к кремлю территориях, и вдоль стен устраивалась зона отдыха горожан с прокладкой прогулочных троп, обсаженных деревьями и кустарниками.</em></p>

<p><em>Памятник легендарному лётчику-испытателю был открыт 15 декабря 1940 года, в день второй годовщины его гибели. Автором скульптуры стал друг В. П. Чкалова И. А. Менделевич, с которым они вместе выбрали это место для памятника А. М. Горькому. И. А. Менделевич был удостоен за эту работу Сталинской премии. Архитекторы памятника: И. Г. Таранов и В. С. Андреев.</em></p>

<p><em>На поверхности пьедестала нанесены контуры карты Северного полушария с указанием маршрутов перелетов героического экипажа Чкалова-Байдукова-Белякова на Дальний Восток и через Северный полюс в Америку. Сам пьедестал облицован лабрадоритом.</em></p>

<p><em>На пьедестале нанесены годы жизни лётчика и надпись &laquo;Валерию Чкалову великому летчику нашего времени&raquo;. Под этими словами, над картой перелётов, видны отверстия от креплений &mdash; там была надпись &laquo;сталинскому соколу&raquo;, удалённая во время борьбы с культом личности Сталина. (из Википедии)</em></p>

<p><br />
<strong>Нижегородская канатная дорога.</strong><br />
Нижегородская канатная дорога (около 3,5 км). Аттракцион носит вполне утилитарный характер. По канатной дороге жители ежедневно катаются из района Бор в центр. Проезд стоит 100 р. Время в пути примерно 12-15 минут. На ветру покачивает. Зимой, когда сильные ветра кабинки сильно раскачивает, бывает так что движение приостанавливают для компенсации раскачивания - тогда по словам местных бывает и стремноватенько.<br />
ТТХ канатной дороги из Википедии:<br />
Тип канатной дороги&nbsp;&mdash; MULTIX GD8, тип гондолы DIAMOND C8S190 с отцепляемым зажимом. Пропускная способность&nbsp;&mdash; около 500 пассажиров в час.<br />
Длина дороги составляет 3661 метр при перепаде высот в 62 метра<a href="https://ru.wikipedia.org/wiki/%D0%9D%D0%B8%D0%B6%D0%B5%D0%B3%D0%BE%D1%80%D0%BE%D0%B4%D1%81%D0%BA%D0%B0%D1%8F_%D0%BA%D0%B0%D0%BD%D0%B0%D1%82%D0%BD%D0%B0%D1%8F_%D0%B4%D0%BE%D1%80%D0%BE%D0%B3%D0%B0#cite_note-gorimpex-4" target="_self">[4]</a>, число опор&nbsp;&mdash; 10.<a href="https://ru.wikipedia.org/wiki/%D0%9D%D0%B8%D0%B6%D0%B5%D0%B3%D0%BE%D1%80%D0%BE%D0%B4%D1%81%D0%BA%D0%B0%D1%8F_%D0%BA%D0%B0%D0%BD%D0%B0%D1%82%D0%BD%D0%B0%D1%8F_%D0%B4%D0%BE%D1%80%D0%BE%D0%B3%D0%B0#cite_note-gorimpex-4" target="_self">[4]</a>&nbsp;Под вокзалы и опоры было выделено 31,8 га в&nbsp;Нижегородском районе и 29,6 га на&nbsp;Бору.&nbsp;<br />
Используется 28 восьмиместных кабин. Проект предусматривает увеличение числа кабин до 56 с повышением пропускной способности до 1000 пассажиров в час. Расчётное время в пути&nbsp;&mdash; 12,5 минут, что соответствует средней скорости в 14&mdash;22 км/ч.<br />
Нижегородская канатная дорога имеет самый большой в Европе безопорный пролёт над водной поверхностью&nbsp;&mdash; 861 метр.<br />
<br />
<a href="https://ic.pics.livejournal.com/drtg6/84837712/7270/7270_original.jpg" target="_blank"><img alt="" src="https://ic.pics.livejournal.com/drtg6/84837712/7270/7270_800.jpg" /></a><br />
<br />
<a href="https://ic.pics.livejournal.com/drtg6/84837712/7464/7464_original.jpg" target="_blank"><img alt="" src="https://ic.pics.livejournal.com/drtg6/84837712/7464/7464_800.jpg" /></a><br />
<br />
<a href="https://ic.pics.livejournal.com/drtg6/84837712/7734/7734_original.jpg" target="_blank"><img alt="" src="https://ic.pics.livejournal.com/drtg6/84837712/7734/7734_800.jpg" /></a><br />
<br />
<a href="https://ic.pics.livejournal.com/drtg6/84837712/8076/8076_original.jpg" target="_blank"><img alt="" src="https://ic.pics.livejournal.com/drtg6/84837712/8076/8076_800.jpg" /></a><br />
<br />
<br />
<strong>Городской округ БОР.</strong><br />
По Бору сделал символический заход к памятнику Ленину и сделал пару фотографий с выставки военной техники.<br />
<br />
<a href="https://ic.pics.livejournal.com/drtg6/84837712/8402/8402_original.jpg" target="_blank"><img alt="" src="https://ic.pics.livejournal.com/drtg6/84837712/8402/8402_800.jpg" /></a><br />
<br />
<a href="https://ic.pics.livejournal.com/drtg6/84837712/8515/8515_original.jpg" target="_blank"><img alt="" src="https://ic.pics.livejournal.com/drtg6/84837712/8515/8515_800.jpg" /></a><br />
<em>Памятник В.И. Ленину в городе Бор Нижегородской области расположен возле ДК &quot;Теплоход&quot; и представляет собой стандартную пятиметровую гипсовую скульптуру, выкрашенную серебрянкой и водруженную на серый цементный постамент.<br />
<br />
Имя автора данного монумента не дошло до благодарных потомков. Однако среди туристов данная скульптура популярна как классический образчик рестайла, повсеместно происходившего в конце тридцатых годов прошлого столетия.&nbsp;<br />
<br />
В первую очередь бросается в глаза совершенно нетипичная для изображений вождя мирового пролетариата поза и одежда, больше напоминающая шинель, нежели привычный нам всем плащ. Очевидны швы, оставшиеся при замене лица вождя. Все становится на свои места, если знать, что изначально скульптура изображала Ф.Э. Дзержинского. Посему памятник имеет не столько художественную, сколько историческую ценность - как наглядный образец воздействия идеологии на культурные объекты.&nbsp;</em>(rutraveller.ru)<br />
Читать полностью на :&nbsp;<a href="https://www.rutraveller.ru/place/26281" target="_self">https://www.rutraveller.ru/place/26281</a><br />
<br />
<a href="https://ic.pics.livejournal.com/drtg6/84837712/8915/8915_original.jpg" target="_blank"><img alt="" src="https://ic.pics.livejournal.com/drtg6/84837712/8915/8915_800.jpg" /></a><br />
<strong>27 сентября</strong><br />
<strong>Ночной дождливый Нижний Новгород.&nbsp;</strong><br />
Конференция работала до 18-00, тем не менее это не помешало мне сделать небольшой пеший ночной трип по городу.<br />
Декатлоновкий плащ-пончо + водонепроницаемые трекинговые мембранные ботинки дают возможность с комфортом передвигаться когда под ногами потоки воды и с верху поливает дождь с вкраплениями снега под шквальным ветром.<br />
<br />
<a href="https://ic.pics.livejournal.com/drtg6/84837712/9191/9191_original.jpg" target="_blank"><img alt="" src="https://ic.pics.livejournal.com/drtg6/84837712/9191/9191_800.jpg" /></a><br />
<br />
<a href="https://ic.pics.livejournal.com/drtg6/84837712/9222/9222_original.jpg" target="_blank"><img alt="" src="https://ic.pics.livejournal.com/drtg6/84837712/9222/9222_800.jpg" /></a><br />
<br />
<a href="https://ic.pics.livejournal.com/drtg6/84837712/9545/9545_original.jpg" target="_blank"><img alt="" src="https://ic.pics.livejournal.com/drtg6/84837712/9545/9545_800.jpg" /></a><br />
Памятник Максиму Горькому в Нижнем Новгороде&nbsp; на площади Горького, был открыт 2 ноября 1952 года. Семиметровая бронзовая фигура Горького создана скульптором В. И. Мухиной, архитектурное решение принадлежит В. В. Лебедеву и П. П. Штеллеру. Скульптура отлита на Ленинградском заводе &laquo;Монументскульптура&raquo;.<br />
<br />
<a href="https://ic.pics.livejournal.com/drtg6/84837712/9882/9882_original.jpg" target="_blank"><img alt="" src="https://ic.pics.livejournal.com/drtg6/84837712/9882/9882_800.jpg" /></a><br />
<em>Малая Покровская пешеходная пешеходная улица.</em><br />
<br />
<strong>28 Сентября 2018</strong><br />
Перед отъездом заглянул в Крестовоздвиженский женский монастырь.<br />
<br />
<a href="https://ic.pics.livejournal.com/drtg6/84837712/9992/9992_original.jpg" target="_blank"><img alt="" src="https://ic.pics.livejournal.com/drtg6/84837712/9992/9992_800.jpg" /></a><br />
<br />
<a href="https://ic.pics.livejournal.com/drtg6/84837712/10450/10450_original.jpg" target="_blank"><img alt="" src="https://ic.pics.livejournal.com/drtg6/84837712/10450/10450_800.jpg" /></a><br />
<br />
<a href="https://ic.pics.livejournal.com/drtg6/84837712/10742/10742_original.jpg" target="_blank"><img alt="" src="https://ic.pics.livejournal.com/drtg6/84837712/10742/10742_800.jpg" /></a><br />
<br />
<a href="https://ic.pics.livejournal.com/drtg6/84837712/10952/10952_original.jpg" target="_blank"><img alt="" src="https://ic.pics.livejournal.com/drtg6/84837712/10952/10952_800.jpg" /></a><br />
<br />
Прогулялся по старым улицам под дождем созерцая старую купеческую застройку.<br />
<br />
<a href="https://ic.pics.livejournal.com/drtg6/84837712/11076/11076_original.jpg" target="_blank"><img alt="" src="https://ic.pics.livejournal.com/drtg6/84837712/11076/11076_800.jpg" /></a><br />
<br />
<a href="https://ic.pics.livejournal.com/drtg6/84837712/11284/11284_original.jpg" target="_blank"><img alt="" src="https://ic.pics.livejournal.com/drtg6/84837712/11284/11284_800.jpg" /></a><br />
<br />
<a href="https://ic.pics.livejournal.com/drtg6/84837712/11635/11635_original.jpg" target="_blank"><img alt="" src="https://ic.pics.livejournal.com/drtg6/84837712/11635/11635_800.jpg" /></a><br />
<br />
<a href="https://ic.pics.livejournal.com/drtg6/84837712/11985/11985_original.jpg" target="_blank"><img alt="" src="https://ic.pics.livejournal.com/drtg6/84837712/11985/11985_800.jpg" /></a><br />
<br />
<br />
До отправления поезда в Москву оставалосьпара часов, и я маханул на метро в тяжелопромышленную часть города до ГАЗа. Горьковский автомобильный завод одно из крупных предприятий города. Тут делали знаменитые ГАЗ66, Волги и другие военные и гражданские автомобили. И сейчас много чего делают. Завод, если смотреть по карте - гигантский.<br />
<br />
<br />
<a href="https://ic.pics.livejournal.com/drtg6/84837712/12052/12052_original.jpg" target="_blank"><img alt="" src="https://ic.pics.livejournal.com/drtg6/84837712/12052/12052_800.jpg" /></a><br />
<a href="https://ic.pics.livejournal.com/drtg6/84837712/12404/12404_original.jpg" target="_blank"><img alt="" src="https://ic.pics.livejournal.com/drtg6/84837712/12404/12404_800.jpg" /></a><br />
<br />
На этом мое знакомство с Нижним Новгородом в рамках этой поездки закончилось. Город мне понравился, не смотря на непрекращающийся дождь, за три неполных дня моего пребывания здесь я прошел около 50 км, протестировал мембранную обувь и дождевое снаряжение, насладился архитектурой домов&nbsp;старой купеческой постройки и прокатился на канатной дороге. В общем провел время с пользой :-)<br />
<br />
Всем удачи и интересных исследований!&nbsp;<br />
Павел Ляхов.</p>
