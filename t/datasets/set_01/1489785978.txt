<h1><strong>Голодный одиночный ВелоПВД &nbsp;пл. Некрасовская &ndash; пл. Шереметьевская, 34 км. (12.03.2017)</strong></h1>

<p><br />
<strong>Маршрут:&nbsp;пл. Некрасовская &ndash; оз. Нерское &ndash; пл. Депо &ndash; Аббакумово &ndash; пл. Шереметьевская</strong><br />
<br />
<em>Я сделал предположение, что при планировании маршрута для его прохождения на фэтбайке по мартовскому&nbsp; смороженному снегу достаточно того, что со спутника просматриваются просеки, которые с большой долей вероятности должны быть не сильно завалены деревьями - в целом мои предположения были верны. Ставка также была сделана на достаточно прочный наст в раннее время суток и на наличие снегоходных следов на участках маршрута, которые я буду проходить после того как воздух прогреется под мартовским солнцем и наст перестанет держать. Часть маршрута до озера Нерского априори была &nbsp;проходима, так как является частью лыжной трассы. Также был поставлен эксперимент с голодным прохождением маршрута.</em><br />
<br />
<a href="http://ic.pics.livejournal.com/pavel_lyakhov/14975806/226852/226852_original.jpg" target="_blank"><img alt="" src="http://ic.pics.livejournal.com/pavel_lyakhov/14975806/226852/226852_800.jpg" style="margin-right:10px" /></a><br />
<br />
Выехал я на пару&nbsp; часов позже запланированного и был на ст. Некрасовская&nbsp; в 9-50. На Некрасовской лыжне было достаточно много лыжников, но я быстро обогнал их, так как передвигался по снежным полям вдоль лыжни по твердому, смороженному снегу. Где-то проезжал по лыжне, там, где лес слишком близко подходил к лыжне.</p>

<p>Прокатился по болоту.</p>

<p><iframe frameborder="0" height="450" src="https://www.youtube.com/embed/G7GL2ypn94M?rel=0" width="800"></iframe></p>

<p><br />
<br />
<a href="http://ic.pics.livejournal.com/pavel_lyakhov/14975806/227311/227311_original.jpg" target="_blank"><img alt="" src="http://ic.pics.livejournal.com/pavel_lyakhov/14975806/227311/227311_800.jpg" style="margin-right:10px" /></a><br />
<br />
<a href="http://ic.pics.livejournal.com/pavel_lyakhov/14975806/227460/227460_original.jpg" target="_blank"><img alt="" src="http://ic.pics.livejournal.com/pavel_lyakhov/14975806/227460/227460_800.jpg" style="margin-right:10px" /></a><br />
<br />
Мартовское солнце светило ярко и хорошо прогревало воздух, а чистый лесной снег добавлял яркости солнечному пейзажу.<br />
Ближе к 12 часам дня снег стал более рыхлым и я вынужден был ехать по буранкам, так как укатанный снегоходами снег был достаточно твердым чтобы удерживать меня на поверхности.<br />
<br />
<a href="http://ic.pics.livejournal.com/pavel_lyakhov/14975806/228168/228168_original.jpg" target="_blank"><img alt="" src="http://ic.pics.livejournal.com/pavel_lyakhov/14975806/228168/228168_800.jpg" style="margin-right:10px" /></a><br />
<br />
На озере Нерском лёд еще стоит и я без проблем на него въехал на него с берега, но сильно далеко отъезжать от берега не стал, так как имею паталогический страх перед весенним льдом. Тем более в одиночку выходить на лед еще более рискованно.<br />
<br />
<a href="http://ic.pics.livejournal.com/pavel_lyakhov/14975806/233385/233385_original.jpg" target="_blank"><img alt="" src="http://ic.pics.livejournal.com/pavel_lyakhov/14975806/233385/233385_800.jpg" style="margin-right:10px" /></a><br />
<br />
<a href="http://ic.pics.livejournal.com/pavel_lyakhov/14975806/227898/227898_original.jpg" target="_blank"><img alt="" src="http://ic.pics.livejournal.com/pavel_lyakhov/14975806/227898/227898_800.jpg" style="margin-right:10px" /></a><br />
<br />
Если до Нерского маршрут был известен и предсказуем, а это чуть меньше чем треть пути, то дальнейший маршрут был для меня новым. К счастью по намеченному мною направлению было множество следов от снегоходов и колеи квадроциклов, и&nbsp; я двигался по ним. Езда по квадроциклетному следу не очень удобна, но в некоторых местах альтернативы не было.<br />
<br />
Отъехав от Нерского я пересек&nbsp; небольшую асфальтовую дорогу и выехал на газопроводную просеку, которая упиралась в Лобню.<br />
<br />
<a href="http://ic.pics.livejournal.com/pavel_lyakhov/14975806/229103/229103_original.jpg" target="_blank"><img alt="" src="http://ic.pics.livejournal.com/pavel_lyakhov/14975806/229103/229103_800.jpg" style="margin-right:10px" /></a><br />
<br />
Про этой просеке ехать было особенно весело, так как она представляла собой скопление маленьких горок по которым сверху проходила лыжня и снегоходка. Пейзаж украшали аномальные березки, расплодившиеся по лесам Подмосковья после декабрьского ледяного дождя.<br />
<br />
<a href="http://ic.pics.livejournal.com/pavel_lyakhov/14975806/229160/229160_original.jpg" target="_blank"><img alt="" src="http://ic.pics.livejournal.com/pavel_lyakhov/14975806/229160/229160_800.jpg" style="margin-right:10px" /></a><br />
<br />
Температура воздуха прогрелась на столько, что при съезде со снегоходки велосипед&nbsp; проваливался в снег, а при спешивании нога уходила выше колена в снег, под которым стояла черная вода.<br />
<br />
<a href="http://ic.pics.livejournal.com/pavel_lyakhov/14975806/229586/229586_original.jpg" target="_blank"><img alt="" src="http://ic.pics.livejournal.com/pavel_lyakhov/14975806/229586/229586_800.jpg" style="margin-right:10px" /></a><br />
<br />
Самое интересное было впереди: после относительно быстрой езды по виражам газопроводной просеки я уперся в долину речки Уча, которая была подтоплена начинающимся половодьем. Местами на речке был еще лёд, но проехать по нему было уже невозможно. Река в месте пересечения с просекой была не сильно широкой &nbsp;- около 5 &nbsp;метров, поэтому отступать было глупо. Были мысли переехать речку вброд на велосипеде, но нагромождение льда делало эту попыткой провальной на 90 процентов и увеличивало шансы завалиться с велосипеда в холодную воду пусть и небольшой, но все-же водной преграды.<br />
В итоге брод был перейден традиционным способом (бегом, в обуви) достаточно быстро, так что берцы по ощущениям не сильно намокли.<br />
<br />
<a href="http://ic.pics.livejournal.com/pavel_lyakhov/14975806/229658/229658_original.jpg" target="_blank"><img alt="" src="http://ic.pics.livejournal.com/pavel_lyakhov/14975806/229658/229658_800.jpg" style="margin-right:10px" /></a><br />
<br />
Ближе к Депо я встретил лыжника Ивана, который был удивлен, увидя велосипедиста на лыжне и еще более удивлен самой конструкции велосипеда с большими колесами. Немного пообщались и поехали дальше. Приятно встретить в лесу общительных единомышленников :-)<br />
<br />
Через некоторое время я свернул с газопровода на тропу в сторону опытных полей института кормов имени Вильямса.<br />
<br />
<a href="http://ic.pics.livejournal.com/pavel_lyakhov/14975806/230016/230016_original.jpg" target="_blank"><img alt="" src="http://ic.pics.livejournal.com/pavel_lyakhov/14975806/230016/230016_800.jpg" style="margin-right:10px" /></a><br />
<br />
И по краю поля по лыжне выехал в населенную зону. К моему удивлению я быстро нашел фарватеры в лабиринтах заборов жилой застройки. Я сфотографировал места калиток с gps координатами (на карте от Strava - в низу статьи они видны).<br />
<br />
<a href="http://ic.pics.livejournal.com/pavel_lyakhov/14975806/230212/230212_original.jpg" target="_blank"><img alt="" src="http://ic.pics.livejournal.com/pavel_lyakhov/14975806/230212/230212_800.jpg" style="margin-right:10px" /></a><br />
<br />
После Вильямсовских полей я пересек Рогачевское шоссе. Здесь &nbsp;была запланированная точка принятия решения. Решение ехать дальше, или сливаться по Рогачевке в Лобню зависило от наличия буранок или других следов по намеченному со спутника направлению. Разведка показала что буранки вдоль ЛЭП есть, да и вообще там хорошо натоптано и пешими и лыжниками, и я принял решение ехать дальше. Переезжая через ручьи я несся вдоль ЛЭП, пока свернул в сторону просеки, идущей в сторону ЖД. Но по просеке была проложенная вялая лыжня, достаточно оттаявшая на солнце, так что при нескольких попытлк по ней проехать, проваливаясь по оси я решил чуть вернуться, и поехал через прилегающий лесопарк станции ДЕПО Савеловской ЖД, где по мосту пересек железную дорогу.<br />
<br />
<a href="http://ic.pics.livejournal.com/pavel_lyakhov/14975806/231366/231366_original.jpg" target="_blank"><img alt="" src="http://ic.pics.livejournal.com/pavel_lyakhov/14975806/231366/231366_800.jpg" style="margin-right:10px" /></a><br />
С моста я провёл разведку местности на пример возможности проезда через населенку к ЛЭП.<br />
<br />
<a href="http://ic.pics.livejournal.com/pavel_lyakhov/14975806/231459/231459_original.jpg" target="_blank"><img alt="" src="http://ic.pics.livejournal.com/pavel_lyakhov/14975806/231459/231459_800.jpg" style="margin-right:10px" /></a><br />
<br />
<br />
<a href="http://ic.pics.livejournal.com/pavel_lyakhov/14975806/231864/231864_original.jpg" target="_blank"><img alt="" src="http://ic.pics.livejournal.com/pavel_lyakhov/14975806/231864/231864_800.jpg" style="margin-right:10px" /></a><br />
<br />
От населенки, прилегающей к Депо по наезженной квадрациклами тропе я выехал на финишную ЛЭП, вдоль которой планировал проехать до Аббакумово.<br />
<br />
<a href="http://ic.pics.livejournal.com/pavel_lyakhov/14975806/232136/232136_original.jpg" target="_blank"><img alt="" src="http://ic.pics.livejournal.com/pavel_lyakhov/14975806/232136/232136_800.jpg" style="margin-right:10px" /></a><br />
<br />
Тут я почувствовал острое чувство голода! У меня была с собой вода я откладывался к фляге. Есть хотелось адски я проклял идею голодных походов! Периодически останавливаясь на отдохнуть я с трудом преодолеоэл эти 7 км ближайшей деревни Аббакумово. Я чувствовал как меня шатает.<br />
<br />
<a href="http://ic.pics.livejournal.com/pavel_lyakhov/14975806/232444/232444_original.jpg" target="_blank"><img alt="" src="http://ic.pics.livejournal.com/pavel_lyakhov/14975806/232444/232444_800.jpg" style="margin-right:10px" /></a><br />
Ощущение слабости слегка напугало меня, но я упорно крутил педали, приближая свое тело к магазину.<br />
<br />
<br />
<a href="http://ic.pics.livejournal.com/pavel_lyakhov/14975806/232665/232665_original.jpg" target="_blank"><img alt="" src="http://ic.pics.livejournal.com/pavel_lyakhov/14975806/232665/232665_800.jpg" style="margin-right:10px" /></a><br />
<br />
Второе дыхание открылось когда я увидел первые дома и местных жителей, у которых узнал координаты ближайшего магазина, куда немедля и помчался.<br />
<br />
Шаурма и кружка чая + кружка кофе быстро вернули мне силы.<br />
<br />
<a href="http://ic.pics.livejournal.com/pavel_lyakhov/14975806/233542/233542_original.jpg" target="_blank"><img alt="" src="http://ic.pics.livejournal.com/pavel_lyakhov/14975806/233542/233542_800.jpg" style="margin-right:10px" /></a><br />
<br />
Отдыхать долго не пришлось, так как в 16-30 надо было быть дома, чтобы успеть сходить на концерт симфонического оркестра&nbsp;#RockestraLive&nbsp;в Кремле:-)<br />
<br />
Пока ехал до пл. Шереметьевская - извозюкал себя и велосипед в послезимней грязи, как будто и не был в снежном лесу :-)<br />
<br />
<a href="http://ic.pics.livejournal.com/pavel_lyakhov/14975806/232763/232763_original.jpg" target="_blank"><img alt="" src="http://ic.pics.livejournal.com/pavel_lyakhov/14975806/232763/232763_800.jpg" style="margin-right:10px" /></a><br />
<br />
На концерт в итоге успел!<br />
<br />
<a href="http://ic.pics.livejournal.com/pavel_lyakhov/14975806/233024/233024_original.jpg" target="_blank"><img alt="" src="http://ic.pics.livejournal.com/pavel_lyakhov/14975806/233024/233024_800.jpg" style="margin-right:10px" /></a><br />
<br />
По поводу голодных походов скажу твердое НЕТ! Еды должно быть много!!!!</p>

<p>&nbsp;</p>

<h2>Видео</h2>

<p><iframe frameborder="0" height="450" src="https://www.youtube.com/embed/kdQ4VQh3qb8" width="800"></iframe></p>

<h2><br />
&nbsp;</h2>

<h2>Ссылка на gps трэки</h2>

<p><br />
Трэк можно скачать здесь (gpx):&nbsp;<a href="https://yadi.sk/d/huxkL_1g3Fub7V">https://yadi.sk/d/huxkL_1g3Fub7V</a></p>

<p>&nbsp;</p>

<p><iframe frameborder="0" height="405" scrolling="no" src="https://www.strava.com/activities/897307587/embed/6d298f94c4bbddf14296a9aecca46e21b4d649c6" width="800"></iframe></p>
