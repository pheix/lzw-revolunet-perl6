use v6.c;
use Test;

use LZW::Revolunet;

my $frlen  = 8192;
my $dspath = './t/datasets';
my $phrase = (
    'Тексты - это не энциклопедические и не лингвистические ' ~
    ' системы. Тексты сужают бесконечные или неопределенные ' ~
    'возможности систем и создают закрытый универсум. Системы ' ~
    'редельны, но бесконечны. Тексты - предельны и конечны, хотя ' ~
    'интерпретаций может быть очень много. Источник - Умберто Эко ' ~
    'От интернета к Гуттенбергу: текст и гипертекст - URL: ' ~
    'http://kiev.philosophy.ru/library/eco/internet.html Точность ' ~
    'цитирования - почти дословно'
) x 255;
my \debug = (
    %*ENV<LZWREVODEBUG>.defined ||
    ( @*ARGS[0].defined && @*ARGS[0] eq 'debug' )
) ?? True !! False;

plan 1;

subtest {
    plan 6;

    my int $r = 0;
    my $obj   = LZW::Revolunet.new(:dictsize(200000));

    is(
        $obj.decode_utf8(:s($obj.encode_utf8(:s($phrase)))),
        $phrase,
        'escape/unescape'
    );
    ok $obj.compress(:s($obj.encode_utf8(:s($phrase)))), 'compress';
    ok $obj.get_ratio > 0, 'compress ratio is set: ' ~ $obj.get_ratio ~ '%';
    ok(
        $obj.decode_utf8(
            :s($obj.decompress(
                :s($obj.compress(:s($obj.encode_utf8(:s($phrase))), :ratio($r)))
            ))
        ),
        'decompress',
    );

    my $cmp = $obj.compress(:s($obj.encode_utf8(:s($phrase))));

    is(
        $obj.decode_utf8(:s($obj.decompress(:s($cmp)))),
        $phrase,
        'compress and decompress, ' ~
            $obj.get_bytes(:s($cmp)) ~ '/' ~ $obj.get_bytes(:s($phrase)) ~
                ' bytes (' ~ $obj.get_ratio ~ '%)'
    );
    ok process_datasets($obj, debug), 'process datasets';
}, 'subtest 1';

done-testing;

sub process_datasets(Any $obj, Bool $debug) returns Bool {
    my @ds   = get_datasets;

    for @ds -> $ds {
        my int $r  = 0;
        my @frms   = dataset_to_frames($ds);
        for @frms -> $f {
            my $cmp  = $obj.compress(:s($obj.encode_utf8(:s($f))));
            $r      += $obj.get_ratio;
            my $dcmp = $obj.decode_utf8(:s($obj.decompress(:s($cmp))));
            if $dcmp ne $f {
                ( '***ERR: compare failure in ' ~ $ds ).say;
                return False;
            }
        }
        if $debug {
            printf("%-43s: %3d%%\n", $ds, floor $r/@frms.elems);
        }
    }

    True;
}

sub dataset_to_frames(str $fname) returns List {
    my @frames;

    if $fname.IO.e {
        my $cnt = $fname.IO.slurp;
        my $ff  = ($cnt.chars / $frlen).floor;
        my $hf  = $cnt.chars % $frlen;
        # ( $ff~','~$hf~','~($hf+$ff*$frlen)~'='~$cnt.chars).say;
        for (0..$ff) {
            if ( $_ < $ff ) {
                @frames.push( $cnt.substr( $_*$frlen, $frlen) );
            } else {
                @frames.push( $cnt.substr( $_*$frlen, $_*$frlen+$hf) );
            }
        }
    }

    @frames;
}

sub get_datasets returns List {
    my @files;
    my $dir  = $dspath;
    my @todo = $dir.IO;

    while @todo {
        for @todo.pop.dir -> $path {
            @files.push($path.Str) if $path.f;
            @todo.push: $path if $path.d;
        }
    }

    @files
}
